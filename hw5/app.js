const express = require('express');
const app = express();
const body_parser = require('body-parser');
const cassandra = require('cassandra-driver');
const multer = require('multer');
const upload = multer();

app.use(body_parser.json());

let client = new cassandra.Client({ contactPoints: ['127.0.0.1'], localDataCenter: 'datacenter1'});
client.connect((err, res) => console.log('cassandra connected'));

app.post('/deposit', upload.single('contents'), (req, res) => {
        console.log('post /deposit');
	console.log(req.body);
	console.log(req.file);
        let upsert_img = 'INSERT INTO hw5.imgs(filename, contents) VALUES(?, ?)';
        client.execute(upsert_img, [req.body.filename, req.file.buffer], {prepare: true}, (err, result) => {
                if (err) console.log(err);
                console.log('img added');
        });
        return res.json({status: "OK"});
});

app.get('/retrieve', (req, res) => {
	console.log('get /retrieve');
	let retrieve_img = 'SELECT * FROM hw5.imgs WHERE filename = ?';
	console.log(req);
	let filename = req.query.filename;
	client.execute(retrieve_img, [filename], {prepare: true}, (err, result) => {
		if (err) return console.log(err);
		console.log('img retrieved');
	        res.type(filename.split('.')[1]);
        	res.end(result.rows[0].contents,'image');
	});
});

app.listen(3000, () => console.log(`Example app listening on port 3000!`));
