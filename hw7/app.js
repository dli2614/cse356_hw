const express = require('express');
const app = express();
const port = 3000;
const mysql = require('mysql');
const Memcached = require('memcached');
const memcached = new Memcached('localhost:11211');


const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'hw7',
	multipleStatements: true
});
connection.connect((err) => {
        if (err) return console.log(err);
        return console.log('Connected to MySQL');
});

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/hw7', (req, res) => {
//	console.log('get /hw7', req.query);
	let club = req.query.club;
	let pos = req.query.pos;
	let key = club + "," + pos;
	let q = `select * from assists where Club=? and POS=? and A=(select max(A) from assists where Club=? and POS=?) order by GS desc, Player asc limit 1; select avg(A) from assists where Club=? and POS=?`;
	memcached.get(key, (err, data) => {
		if (err || !data) {
//			console.log('Cache Miss');
			connection.query(q, [club, pos, club, pos, club, pos], (err, results) => {
				let value = {
                                        club: results[0][0]['Club'],
                                        pos: results[0][0]['POS'],
                                        max_assists: results[0][0]['A'],
                                        player: results[0][0]['Player'],
                                        avg_assists: results[1][0]['avg(A)']
                                }
				res.json(value);
				memcached.set(key, value, 10, (err) => {
					if (err) console.error(err);
				});
	        	});
		} else {
//			console.log('Cache Hit');
			res.json(data);
		}
	});
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
